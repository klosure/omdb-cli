PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
MANDIR ?= /usr/share/man/man1

.POSIX: install

all: help

help:
	@echo "please run 'make install' as root"
install:
	cp ./omdb.py $(BINDIR)/omdb
	cp ./omdb.1 $(MANDIR)
uninstall:
	rm $(BINDIR)/omdb
	rm $(MANDIR)/omdb.1
