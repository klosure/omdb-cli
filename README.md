# omdb-cli

[![License:LGPLv3](https://is.gd/eBwIHR)](https://www.gnu.org/licenses/lgpl-3.0)

This is my fork of [bgr/omdb-cli][omdb]. I've included some changes in the way results are output to stdout. I wanted to tweak a few things to be more to my liking. 

### Prerequisites:

You need to obtain your personal **API key** from the [OMDb API
website][omdbapi] in order to be able to use the tool. Once you have it, you
can either pass it via `--apikey` argument, or if you don't wish to pass it
every time, you can set it as an environment variable `OMDB_API_KEY` (recommended).

> Depends on: make, python3

### Install:
```bash
git clone https://gitlab.com/klosure/omdb-cli.git
cd omdb-cli
sudo make install
```

### Usage

Show all info about the movie 'Cars'

    omdb -t Cars

Show all info about the series 'Firefly'

    omdb -t firefly --type series

Show all info about season 1 episode 1 of 'Firefly'

    omdb -t firefly --season 1 --episode 1

Show all info about the series 'Firefly', in JSON format

    omdb -t firefly -r JSON

Show all info about latest 'True Grit' movie

    omdb -t "True Grit"

Show all info about 1969 version of 'True Grit'

    omdb -t "True Grit" -y 1969

Print movie's rating only

    omdb -t Cars | sed -n '/^imdbrating/{n;p;}'

Download movie's poster

    omdb -t Cars | wget `sed -n '/^poster/{n;p;}'`


### Additional useful features:

Include full plot summary (not available for some movies)

    omdb -t "True Grit" --plot full

Include additional data from Rotten Tomatoes

    omdb -t "True Grit" --tomatoes

Show info by IMDb id

    omdb -i tt0103064


### Example to get ratings for all movies in current directory

(it will use directory and file names as movie titles)

Save following code to file `get_ratings.sh` (make sure to update the path in line 3):

    ls -1 |
    while read title; do
      res=`python /path/to/omdbtool.py -t "$title"`
      rating=`echo "$res" | sed -n '/^imdbrating/{n;p;}'`
      restitle=`echo "$res" | sed -n '/^title/{n;p;}' | sed s/*//g`
      year=`echo "$res" | sed -n '/^year/{n;p;}'`
      echo "$title  *  $restitle  *  $year  *  $rating"
    done

then execute the saved command to fetch all the ratings: `./get_ratings.sh > ratings.txt`
(it'll take a while to retrieve all the data). Then you can open the
`ratings.txt` file to see the movie ratings, or you can sort the movies by
ratings to pick the best one to watch: `< ratings.txt sort -t* -k4 -r`.

_(This used to be more useful when the API worked with inexact movie titles)_

## TODO ##

 - Fix CSV output 
 - Add a output file option
 - Added -v for the api version (The only api out right now is V1 but V2 seems to be on it's way)
 - Clean up the code
 - Package both packages into exes with pyinstaller
 - ~~Add color to cli~~

## Notes ##

 - works with Python 3
 - **thanks to the creator of this [great site called OMDBAPI][omdbapi]**
 - thanks to the original creator of this tool who open sourced it!


## License ##

This tool is licensed under [GNU Lesser GPL][lgpl] license.


[omdbapi]: http://www.omdbapi.com
[lgpl]: http://www.gnu.org/licenses/lgpl.html
[gooey]: https://github.com/chriskiehl/Gooey
[omdb]: https://github.com/bgr/omdb-cli
